<?php
use Ratchet\Server;
use NathemWS\NathemWSS;


    require dirname(__DIR__) . '/vendor/autoload.php';

    $nathemWSS = new NathemWSS('server1', 'mykey');



$server = Server\IoServer::factory(
    new \Ratchet\Http\HttpServer(
        new \Ratchet\WebSocket\WsServer(
            $nathemWSS
        )
    ),

    8080
);



    $server->run();

