<?php
namespace NathemWS;

use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;


class NathemWSS implements MessageComponentInterface {
    protected $clients;
    protected $waitingRequests;
    protected $proxiedRequests;
    private $currentClientId = 1;
    private $responseTypes;
    private $key;
    private $name;

    public function __construct($name, $key) {
        $this->log("Starting NathemWSS...");
        $this->clients = new \SplObjectStorage();
        $this->responseTypes = new \SplObjectStorage();
        $this->waitingRequests = new \SplObjectStorage();
        $this->proxiedRequests = new \SplObjectStorage();
        $this->key = $key;
        $this->name = $name;

        $this->registerHandler("PING", "\\NathemWS\\PingHandler");
    }

    public function onOpen(ConnectionInterface $conn) {
        $client = new NathemWSSClient($this, $conn);
        $this->clients->attach($client);
        // Auth request
        $this->log("Connected", $client);
        $this->sendRequest(new AuthRequest(), $client);

    }

    public function onMessage(ConnectionInterface $from, $msg) {
        $client = $this->getClient($from);
        $arrayMsg = json_decode($msg, true);
        if(!$arrayMsg) return;
        $uuid = $arrayMsg['uuid'];
        $type = $arrayMsg['type'];
        $data = $arrayMsg['data'];

        if(!$client->isAuth() && $type != 'AUTH')
        {
            $this->kickClient($client);
            return;
        }


        // Received responses
        foreach($this->waitingRequests as $waitingRequest)
        {
            if($waitingRequest->getUuid() == $uuid)
            {
                $waitingRequest->receive($data);
                return;
            }
        }

        // Proxy responses
        foreach($this->proxiedRequests as $proxiedRequest)
        {
            if($proxiedRequest->getTargetUuid() == $uuid)
            {

                $proxiedRequest->receive($data);
                return;
            }
        }

        // Proxy requests to handle
        if($type == 'PROXY')
        {
            $target = $this->getClientByName($data['client']);

            if($target == null) {
                return;
            }
            new ProxiedRequest($client, $uuid, $target, $data);
            return;
        }

        // requests to handle
        foreach ($this->responseTypes as $responseType) {

            if ($responseType->isResponseOf($type)) {
                $responseType->newHandler($client, $uuid, $type, $data);
            }
        }
    }

    public function onClose(ConnectionInterface $conn) {
        $client = $this->getClient($conn);
        $this->clients->detach($client);
        $this->log("Disconnect", $client);
    }

    public function onError(ConnectionInterface $conn, \Exception $e) {
        $client = $this->getClient($conn);
        $this->log("An error occured", $client);
        $client->closeConnexion();
    }

    public function getClient(ConnectionInterface $conn)
    {
        foreach ($this->clients as $client) {
            if($client->getConnexion() === $conn) return $client;
        }
        return null;
    }

    public function generateClientId()
    {
        return $this->currentClientId++;
    }

    public function registerHandler($name, $class)
    {
        $this->responseTypes->attach(new NathemWSHandlerType($name, $class));
    }

    public function log($message, NathemWSSClient $client = null)
    {
        $date = date('d M H:i:s');

        if(!$client) echo "[{$date}] ".$message."\n";
        elseif($client->isAuth()) echo "[{$date}] [{$client->getName()}] ".$message."\n";
        else echo "[{$date}] [anonymous] ".$message."\n";
    }

    public function getClientByName($name)
    {
        foreach ($this->clients as $client) {
            if($client->getName() == $name) return $client;
        }
        return null;
    }


    public function removeWaitingRequest(NathemWSRequest $request)
    {
        $this->waitingRequests->detach($request);
    }

    public function sendRequest(NathemWSRequest $request, $client)
    {
        if(!($client instanceof NathemWSSClient))
        {
            $client = $this->getClientByName($client);
        }

        if(!$client){
            $this->log("Unknow client");
            return;
        }

        $request->setClient($client);
        $this->waitingRequests->attach($request);
        $request->send();
        return $request;
    }

    public function kickClient(NathemWSSClient $client)
    {
        $client->closeConnexion();
    }

    /**
     * @return mixed
     */
    public function getKey()
    {
        return $this->key;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    public function addProxiedRequest(ProxiedRequest $request)
    {
        $this->proxiedRequests->attach($request);
    }

    /**
     * @return \SplObjectStorage
     */
    public function getProxiedRequests()
    {
        return $this->proxiedRequests;
    }










}