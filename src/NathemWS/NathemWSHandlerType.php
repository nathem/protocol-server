<?php
namespace NathemWS;


class NathemWSHandlerType{

    private $handlerClass;
    private $name;

    function __construct($name, $handlerClass)
    {
        $this->name = $name;
        $this->handlerClass = $handlerClass;
    }


    public function newHandler(NathemWSSClient $client, $uuid, $type, $requestData)
    {
        return new $this->handlerClass($client, $uuid, $type, $requestData);
    }

    public function isResponseOf($type)
    {
        return $type == $this->name;
    }
}