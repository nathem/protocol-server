<?php
namespace NathemWS;
use Ratchet\ConnectionInterface;

class NathemWSSClient{

    protected $id;
    protected $connexion;
    protected $isAuth;
    protected $server;
    protected $name;

    public function __construct(NathemWSS $server, ConnectionInterface $connexion) {
        $this->server = $server;
        $this->id = $server->generateClientId();
        $this->connexion = $connexion;
        $this->isAuth = false;

    }

    public function getConnexion()
    {
        return $this->connexion;
    }

    public function isAuth()
    {
        return $this->isAuth;
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }



    public function closeConnexion()
    {
        $this->connexion->close();
    }

    public function send($message)
    {
        $this->connexion->send($message);
    }

    public function auth($name)
    {
        if($this->getServer()->getClientByName($name)) $this->getServer()->kickClient($this);
        $this->name = $name;
        $this->isAuth = true;
        $this->server->log("Authenticated", $this);

    }

    /**
     * @return NathemWSS
     */
    public function getServer()
    {
        return $this->server;
    }





}