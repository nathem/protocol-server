<?php

namespace NathemWS;


class PingRequest extends NathemWSRequest {

    public function getType()
    {
        return 'PING';
    }

    public function buildData()
    {
        return new \stdClass();
    }

    public function onResponse($data)
    {
    }
}