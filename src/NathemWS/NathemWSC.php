<?php


namespace NathemWS;


use React\EventLoop\Factory;
use WebSocket\Client;


class NathemWSC
{

    private $host;
    private $key;
    private $clientName;
    private $client;

    function __construct($host, $key, $clientName)
    {
        $this->host = $host;
        $this->key = $key;
        $this->clientName = $clientName;

        $this->client = new WebsocketClient("ws://".$host);
        $this->auth();


    }

    public function sendRequest(NathemWSRequest $request, $client)
    {
        $uuid = $this->send('PROXY', array(
            'data' => $request->buildData(),
            'client' => $client,
            'type' => $request->getType(),
        ));

        return $this->receiveUUID($uuid)['data'];
    }

    private function auth()
    {

        $data = $this->receive('AUTH');

        $this->send('AUTH', array(
            'key' => $this->key,
            'name' => $this->clientName,

        ), $data['uuid']);

    }

    private function send($type, $data, $uuid = null)
    {
        if ($uuid == null) $uuid = md5(uniqid(rand(), true));
        $array = array(
            'uuid' => $uuid,
            'type' => $type,
            'data' => $data,
        );

        $this->client->send(json_encode($array));
        return $uuid;
    }


    public function receive($type){

        while(true)
        {
            $message = $this->client->receive();
            $data = json_decode($message, true);
            if($data['type'] == $type)
            {return $data;}
        }
    }

    public function receiveUUID($uuid){

        while(true)
        {
            $message = $this->client->receive();
            $data = json_decode($message, true);
            if($data['uuid'] == $uuid)
            {return $data;}
        }
    }

    public function stop()
    {
        $this->client->close();
    }


}


