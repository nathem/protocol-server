<?php
namespace NathemWS;

abstract class NathemWSHandler{

    protected $uuid;
    protected $client;
    protected $data;
    protected $type;

    public function __construct(NathemWSSClient $client, $uuid, $type, $data) {
        $this->data = $data;
        $this->type = $type;
        $this->client = $client;
        $this->uuid = $uuid;
        $this->send();
    }

    /**
     * @return string
     */
    public function getUuid()
    {
        return $this->uuid;
    }


    abstract protected function handle($data);
    abstract protected function onResponseSent();



    protected function send()
    {
        $response = array(
            'uuid' => $this->uuid,
            'type' => $this->type,
            'data' => $this->handle($this->data),
        );
        $this->client->send(json_encode($response));
        $this->client->getServer()->log($this->type ." response sent", $this->client);
        $this->onResponseSent();

    }











}