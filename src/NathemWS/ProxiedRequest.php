<?php

namespace NathemWS;


class ProxiedRequest{

    private $uuid;
    private $sender;
    private $target;
    private $type;
    private $data;
    private $targetUuid;

    function __construct(NathemWSSClient $sender, $uuid, NathemWSSClient $target, $data)
    {
        $this->sender = $sender;
        $this->target = $target;
        $this->uuid = $uuid;
        $this->targetUuid = md5(uniqid(rand(), true));
        $this->type = $data['type'];
        $this->data = $data['data'];
        $this->sendRequest();
    }

    function sendRequest()
    {
        $request = array(
            'uuid' => $this->targetUuid,
            'type' => $this->type,
            'data' => $this->data,
        );

        $this->getServer()->addProxiedRequest($this);
        $msg = utf8_encode(json_encode($request));
        $this->target->send($msg);
        $this->getServer()->log("Proxied request received, ".$this->type ." request sent", $this->sender);
    }

    function receive($data)
    {
        $request = array(
            'uuid' => $this->uuid,
            'type' => 'PROXY',
            'data' => $data,
        );
        $this->sender->send(json_encode($request));

        $this->getServer()->log($this->type ." response received, proxied response sent", $this->sender);

    }


    public function getServer()
    {
        return $this->sender->getServer();
    }

    /**
     * @return string
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * @return NathemWSSClient
     */
    public function getSender()
    {
        return $this->sender;
    }

    /**
     * @return NathemWSSClient
     */
    public function getTarget()
    {
        return $this->target;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @return string
     */
    public function getTargetUuid()
    {
        return $this->targetUuid;
    }



}