<?php

namespace NathemWS;


class AuthRequest extends NathemWSRequest {

    public function getType()
    {
        return 'AUTH';
    }

    public function buildData()
    {
        return array(
            'server' => $this->getServer()->getName(),
        );
    }

    public function onResponse($data)
    {
        $key = $data['key'];
        $clientName = $data['name'];

        if($key == $this->client->getServer()->getKey())
        {
            $this->client->auth($clientName);
        }
        else
        {
            $this->client->getServer()->kickClient($this->client);
            $this->getServer()->log("Authentication failed", $this->client);
        }

    }
}