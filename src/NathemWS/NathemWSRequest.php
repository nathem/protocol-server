<?php

namespace NathemWS;


abstract class NathemWSRequest {

    protected $uuid;
    protected $client;
    protected $responseData;
    protected $waiting;

    function __construct()
    {
        $this->waiting = true;
        $this->uuid = md5(uniqid(rand(), true));

    }

    abstract public function getType();

    abstract public function buildData();

    abstract public function onResponse($data);

    public function send()
    {
        $request = array(
            'uuid' => $this->uuid,
            'type' => $this->getType(),
            'data' => $this->buildData(),
        );

        $msg = utf8_encode(json_encode($request));
        $this->client->send($msg);
        $this->getServer()->log($this->getType() ." request sent", $this->client);

    }

    public function receive($data)
    {
        $this->client->getServer()->removeWaitingRequest($this);
        $this->responseData = $data;
        $this->waiting = false;
        $this->getServer()->log($this->getType() ." response received", $this->client);
        $this->onResponse($data);

    }

    /**
    * @return boolean
    */
    public function isWaiting()
    {
        return $this->waiting;
    }

    /**
     * @return mixed
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * @param NathemWSSClient $client
     */
    public function setClient($client)
    {
        $this->client = $client;
    }

    /**
     * @return NathemWSS
     */
    protected function getServer()
    {
        return $this->client->getServer();
    }

    /**
     * @return mixed
     */
    public function getResponseData()
    {
        return $this->responseData;
    }

    public function waitForResponse($timeout = 20)
    {
        set_time_limit($timeout);
        while($this->waiting){}
        return $this->responseData;
    }

    public function getResponse()
    {
        while($this->waiting){}
        return $this->responseData;
    }










}